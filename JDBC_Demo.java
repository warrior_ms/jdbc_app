package jdbcdemo;

import java.sql.*;
import java.util.Scanner;

public class JDBC_Demo {

 public static void main(String[] args) {

  Scanner myscan = new Scanner(System.in);
  
  System.out.println("Enter Notes category");
  String notes_cat = myscan.next();
  
  System.out.println("Enter Notes content");
  String notes_content = myscan.next();
  
  // Exception handling, because in case if connection fails to succeed, it can be handled.
  try{
   Class.forName("com.mysql.jdbc.Driver");	// Loading driver of MySQL
   
   Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/NotesDB", "root", "root");   // Connection establishment
   
   PreparedStatement query_str = con.prepareStatement("insert into Notes(notes_cat, notes_content) values(?, ?)");
   
   query_str.setString(1, notes_cat);		// Inserting parameters in the prepared statement
   query_str.setString(2, notes_content);

   int count = query_str.executeUpdate();	// The value of count denotes the number of records inserted. If value = 1, 1 record inserted, else it would be 0, denoting no insertion of record.
   
   if(count>0)
    System.out.println("Record Inserted");
   else
    System.out.println("Record Not Inserted");
   
   con.close();
  }

  catch(Exception e)
  {
   e.printStackTrace();
  }

 } 
}


// DB => NotesDB
// Table => Notes(notes_id int primary key auto_increment, notes_cat varchar(20), notes_content varchar(150) not null)
